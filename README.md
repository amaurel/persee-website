# Persee website

## https://infinite-scrubland-81046.herokuapp.com/

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Setup heroku commands

```
heroku login

heroku create

heroku buildpacks:add heroku/nodejs

heroku buildpacks:add https://github.com/heroku/heroku-buildpack-static

npm run build

node server.js
```

### push CI

```
add

commit

git push heroku master
```
