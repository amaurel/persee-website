import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [
      {
        id: '1',
        title: 'HYDROGEN INFRASTRUCTURE FORWARD PLANNING',
        content:
          'TedHy is a comprehensive planning tool for hydrogen projects and infrastructure which question technology, dimensioning, timing, and location choices. tedHy will help you make strategic decision by simulating the best options available for you.',
        logo: require('./../assets/products/tedhy.png'),
        pdf:
          'https://www.canva.com/design/DAD8V89l6Mc/6zctXf-fzfF5I9FcfC89dw/edit#1',
      },
      {
        id: '2',
        title: 'HYDROGEN OPERATIONS SMART SUPERVISION',
        content:
          'SopHy is a real-time "meso" supervision platform blending automation, IoT, data science and visualisation. sopHy will guide you on a journey of continuous optimisation of your hydrogen operations.',
        logo: require('./../assets/products/sophy.png'),
        pdf:
          'https://www.canva.com/design/DAD8V89l6Mc/6zctXf-fzfF5I9FcfC89dw/edit#2',
      },
      {
        id: '3',
        title: 'A STEPPING STONE HYDROGEN MOBILITY',
        content:
          'MobHy is your entry door to hydrogen mobility providing easy to browse vehicule information and general hydrogen mobility knowledge. MobHy will help you start your adoption journey.',
        logo: require('./../assets/products/mobhy.png'),
        pdf:
          'https://www.canva.com/design/DAD8V89l6Mc/6zctXf-fzfF5I9FcfC89dw/edit#3',
      },
      {
        id: '4',
        title: 'HRS NETWORK MANAGEMENT',
        content:
          'FilHy is a smart plug-in to off-the-shelf refueling management solutions providing advanced control features and extensive services to enhance the management of HRS networks.',
        logo: require('./../assets/products/filhy.png'),
        pdf:
          'https://www.canva.com/design/DAD8V89l6Mc/6zctXf-fzfF5I9FcfC89dw/edit#4',
      },
    ],
    team: [
      {
        id: '1',
        name: 'Adrien',
        job: 'Full-stack developer',
        desc:
          "The technical challenges set by Persee's ambitious projects, the compelling context of the transition toward greener energies and the dynamic people and companies working on this field make for a very positive and stimulating working environment.",
        puzzle: require('./../assets/team/p1.png'),
      },
      {
        id: '2',
        name: 'Colin',
        job: '',
        desc:
          'Hydrogen solutions are perfect combinations of complex physical understandings, technical innovations and a worthwhile commitment towards energy transition. Add on top of that a young, enthusiastic, international team and you will get a fulfilling engineering mission.',
        puzzle: require('./../assets/team/p2.png'),
      },
      {
        id: '3',
        name: 'Gloria',
        job: '',
        desc:
          'Accipienda longe rogemus fateatur peccatis ut rogati publicae ceteris rogemus et ut amici Fanni ut et et longe Deflexit sanciatur.',
        puzzle: require('./../assets/team/p3.png'),
      },
      {
        id: '4',
        name: 'He',
        job: 'IoT',
        desc:
          "It can't be more exciting that you can use all the knowledge you have learned to solve the problem that worries you the most. As an electronics and automation engineer, working at Persee offers me this opportunity to make my contribution to the energy transition. It is an adventure with the people who hold the same faith with you.",
        puzzle: require('./../assets/team/p4.png'),
      },
      {
        id: '5',
        name: 'Juba',
        job: 'Devops',
        desc:
          'Accipienda longe rogemus fateatur peccatis ut rogati publicae ceteris rogemus et ut amici Fanni ut et et longe Deflexit sanciatur.',
        puzzle: require('./../assets/team/p5.png'),
      },
      {
        id: '6',
        name: 'Laurence',
        job: 'Founder',
        desc:
          'Trying to make a change which the current situation has proved to be a necessity whilst building an amazing team.',
        puzzle: require('./../assets/team/p6.png'),
      },
      {
        id: '7',
        name: 'Mahadi',
        job: 'Software engineer',
        desc:
          'As a developer, my experience at Persee is no other than positive, the various projects in which we intervene constitute the ideal environment to sharpen your skills and acquire new ones.',
        puzzle: require('./../assets/team/p7.png'),
      },
      {
        id: '8',
        name: 'Nico',
        job: '',
        desc:
          'Persee is the catalyst of the success of hydrogen projects. Its team is great and its expertise is unique as a thinker, conceiver and maker. When I see the enormous potential of Persee and the future impact of hydrogen on our environment, I feel grateful and proud to be one of its shareholders.',
        puzzle: require('./../assets/team/p8.png'),
      },
      {
        id: '9',
        name: 'Pimprenelle',
        job: '',
        desc:
          'I am happy to join the adventure to discover the varied and complex challenges of hydrogen and to use the breadth of mathematical technics to optimise the energy transition.',
        puzzle: require('./../assets/team/p9.png'),
      },
      {
        id: '10',
        name: 'Sezin',
        job: '',
        desc:
          'I find it fascinating that the most promising solution to the gravest energy crisis ever is the lightest, most modest element in the universe. As a researcher, working at Persee gives me the rare opportunity of being on the right side of history. Here I believe that I contribute to a good cause while being surrounded by the most passionate, talented and creative team - a true privilege',
        puzzle: require('./../assets/team/p4.png'),
      },
      {
        id: '11',
        name: 'Tom',
        job: 'Full-stack junior developer',
        desc:
          'Accipienda longe rogemus fateatur peccatis ut rogati publicae ceteris rogemus et ut amici Fanni ut et et longe Deflexit sanciatur.',
        puzzle: require('./../assets/team/p5.png'),
      },
      {
        id: '12',
        name: 'Valentina',
        job: 'Marketing director',
        desc:
          'I believe hydrogen is a game-changer allowing the penetration of a renewable energy mix and working at Persee allows me to be part of many innovative projects that will certainly fast-track and make the energy transition a reality.',
        puzzle: require('./../assets/team/p6.png'),
      },
    ],
  },
  getters: {
    products: state => state.products,
    team: state => state.team,
  },
})
