import Vue from 'vue'
import VueRouter from 'vue-router'

import Persee from '@/components/Persee.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'persee',
    component: Persee,
  },
]

const router = new VueRouter({
  routes,
})

export default router
